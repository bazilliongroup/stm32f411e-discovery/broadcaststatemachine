/*
Library:			BroadcastStateMachine.c
Written by:			Ronald Bazillion
Date written:			09/07/2019
Description:			Implementation File for Broadcast State Machine
Resources:                      https://www.youtube.com/watch?v=pxaIyNbcPrA&index=9&list=PL5701E4325336503E&t=53s
                                https://www.youtube.com/playlist?list=PLn8PRpmsu08pHsOznk_WT6QT9cySw1A59
                                https://help.ubidots.com/en/articles/1161418-mqtt-finite-state-machines-and-ubidots-part-i
                                https://ubidots.com/blog/advantages_and_disadvantages_of_finite_state_machines/amp/?fbclid=IwAR13Hoih8N9JjepKhloYYnhRqDKZTXiNmeo3pCk-oGfVgSVG3g3hrMFKDdo
            
Modifications:
-----------------

*/

//***** Header files *****//
#include "BroadcastStateMachine.h"
#include "TimerModule.h"
#include "UserButton.h"
#include "Leds.h"
#include "main.h"
#include "UartProcessing.h"
#include <string.h> 

extern UART_COMMANDS broadcastUartValue;

//***** Defines ********//
//#define DEBUG
#define QUARTER_SECOND 250
#define HALF_SECOND 500
#define ONE_SECOND 1000

//***** typedefs enums *****//


//***** typedefs structs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//

static void m_gotoRedState1();
static void m_gotoBlueState1();

static void m_machine1State1Event1(void);
static void m_machine1State1Event2(void);
static void m_machine1State2Event1(void);
static void m_machine1State2Event2(void);
static void m_machine1State3Event1(void);
static void m_machine1State3Event2(void);
static void m_machine1State4Event1(void);
static void m_machine1State4Event2(void);

static void m_machine2State1Event1(void);
static void m_machine2State1Event2(void);
static void m_machine2State2Event1(void);
static void m_machine2State2Event2(void);
static void m_machine2State3Event1(void);
static void m_machine2State3Event2(void);
static void m_machine2State4Event1(void);
static void m_machine2State4Event2(void);

static void m_machine3State1Event1(void);
static void m_machine3State1Event2(void);
static void m_machine3State2Event1(void);
static void m_machine3State2Event2(void);
static void m_machine3State3Event1(void);
static void m_machine3State3Event2(void);
static void m_machine3State4Event1(void);
static void m_machine3State4Event2(void);

static void m_machine4State1Event1(void);
static void m_machine4State1Event2(void);
static void m_machine4State2Event1(void);
static void m_machine4State2Event2(void);
static void m_machine4State3Event1(void);
static void m_machine4State3Event2(void);
static void m_machine4State4Event1(void);
static void m_machine4State4Event2(void);


//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//
static UART_HandleTypeDef* m_huart;

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

STATE_MACHINE_STRUCT SM_StateMachineRed;   //global variable used for State Machine; must be extern to use.
STATE_MACHINE_STRUCT SM_StateMachineBlue;   //global variable used for State Machine; must be extern to use.
STATE_MACHINE_STRUCT SM_StateMachineGreen;   //global variable used for State Machine; must be extern to use.
STATE_MACHINE_STRUCT SM_StateMachineOrange;   //global variable used for State Machine; must be extern to use.

//table that processes events. the table is an array of function pointers.
FUNCT_PTR stateEventTableMachine1[MAX_STATES_MACH1][MAX_EVENTS_MACH1] =
{
  { m_machine1State1Event1, m_machine1State1Event2 }, /* procedures for state1 */
  { m_machine1State2Event1, m_machine1State2Event2 }, /* procedures for state2 */
  { m_machine1State3Event1, m_machine1State3Event2 }, /* procedures for state3 */
  { m_machine1State4Event1, m_machine1State4Event2 }  /* procedures for state4 */
};

FUNCT_PTR stateEventTableMachine2[MAX_STATES_MACH2][MAX_EVENTS_MACH2] =
{
  { m_machine2State1Event1, m_machine2State1Event2}, /* procedures for state1 */
  { m_machine2State2Event1, m_machine2State2Event2}, /* procedures for state2 */
  { m_machine2State3Event1, m_machine2State3Event2}, /* procedures for state3 */
  { m_machine2State4Event1, m_machine2State4Event2}  /* procedures for state4 */
};

FUNCT_PTR stateEventTableMachine3[MAX_STATES_MACH3][MAX_EVENTS_MACH3] =
{
  { m_machine3State1Event1, m_machine3State1Event2 }, /* procedures for state1 */
  { m_machine3State2Event1, m_machine3State2Event2 }, /* procedures for state2 */
  { m_machine3State3Event1, m_machine3State3Event2 }, /* procedures for state3 */
  { m_machine3State4Event1, m_machine3State4Event2 }  /* procedures for state4 */
};

FUNCT_PTR stateEventTableMachine4[MAX_STATES_MACH4][MAX_EVENTS_MACH4] =
{
  { m_machine4State1Event1, m_machine4State1Event2}, /* procedures for state1 */
  { m_machine4State2Event1, m_machine4State2Event2}, /* procedures for state2 */
  { m_machine4State3Event1, m_machine4State3Event2}, /* procedures for state3 */
  { m_machine4State4Event1, m_machine4State4Event2}  /* procedures for state4 */
};

void SM_Init(UART_HandleTypeDef* huart)
{  
  m_huart = huart;
  UserButtonInit();
  UserButtonDelayEnable(3000);
  LedInit();
  LedRedTurnOn();
  LedBlueTurnOn();
  LedGreenTurnOn();
  LedOrangeTurnOn();
  
  const char* str = "Mode 1 Red State\r\n\n";
  size_t strSize = strlen(str);
  HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);  
  
  SM_StateMachineRed.States = RED_STATE1_M1;
  SM_StateMachineRed.FuncPtr = SM_State1Machine1;
  
  SM_StateMachineBlue.States = BLUE_STATE1_M2;
  SM_StateMachineBlue.FuncPtr = SM_State1Machine2;
  
  SM_StateMachineGreen.States = GREEN_STATE1_M3;
  SM_StateMachineGreen.FuncPtr = SM_State1Machine3;
    
  SM_StateMachineOrange.States = ORANGE_STATE1_M4;
  SM_StateMachineOrange.FuncPtr = SM_State1Machine3;  
}

void SM_CommonTasks(void)
{
  UserButtonCheck();  
}

//STATE MACHINE 1

void SM_State1Machine1(void)
{       
    if (!UserButtonIsDelayed())
    {
        if (UserButtonGetPressed() && UserButtonGetDbn())
        {
            UserButtonSetPressed(false);
            UserButtonSetDbn(false);
            UserButtonStartDelay();
            stateEventTableMachine1[RED_STATE1_M1][EVENT1_M1]();    //event 1 state1 machine1
        }
    
        //event2 state1 machine1 does nothing
    }
}

void SM_State2Machine1(void)
{
    if (!UserButtonIsDelayed())
    {  
        if (UserButtonGetPressed() && UserButtonGetDbn())
        {
            UserButtonSetPressed(false);
            UserButtonSetDbn(false);
            UserButtonStartDelay();
            stateEventTableMachine1[RED_STATE2_M1][EVENT1_M1]();    //event1 state 2 machine1
        }
        
        if (broadcastUartValue == UART_RESET_RED)
        {
            UserButtonStartDelay();
            stateEventTableMachine1[RED_STATE2_M1][EVENT2_M1]();    //event2 state2 machine1
        }
    }
}

void SM_State3Machine1(void)
{    
    if (!UserButtonIsDelayed())
    {  
        if (UserButtonGetPressed() && UserButtonGetDbn())
        {
            UserButtonSetPressed(false);
            UserButtonSetDbn(false);
            UserButtonStartDelay();
            stateEventTableMachine1[RED_STATE3_M1][EVENT1_M1]();    //event1 state3 machine1
        }
        
        if (broadcastUartValue == UART_RESET_RED)
        {
            UserButtonStartDelay();
            stateEventTableMachine1[RED_STATE3_M1][EVENT2_M1]();    //event2 state3 machine1
        }
    }
}

void SM_State4Machine1(void)
{    
    if (!UserButtonIsDelayed())
    {
        if (UserButtonGetPressed() && UserButtonGetDbn())
        {
            UserButtonSetPressed(false);
            UserButtonSetDbn(false);
            UserButtonStartDelay();
            stateEventTableMachine1[RED_STATE4_M1][EVENT1_M1]();    //event1 state4 machine1
        }  
          
        if (broadcastUartValue == UART_RESET_RED)
        {
            UserButtonStartDelay();
            stateEventTableMachine1[RED_STATE4_M1][EVENT2_M1]();    //event2 state4 machine1
        }    
    }
}

//STATE MACHINE 2

void SM_State1Machine2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        stateEventTableMachine2[BLUE_STATE1_M2][EVENT1_M2]();   //event1 state1 machine2
    }  
    
    //event2 state1 machine2 state1 does nothing
}

void SM_State2Machine2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        stateEventTableMachine2[BLUE_STATE2_M2][EVENT1_M2]();   //event1 state2 machine2
    }    
        
    //TODO event2 state2 machine2 add in UART Blue Reset receive      
}

void SM_State3Machine2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        stateEventTableMachine2[BLUE_STATE3_M2][EVENT1_M2]();   //event1 state3 machine2
    }  
    
    //TODO event2 state3 machine2 add in UART Blue Reset receive
}

void SM_State4Machine2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        stateEventTableMachine2[BLUE_STATE4_M2][EVENT1_M2]();   //event1 state4 machine2
    }    
    if (broadcastUartValue == UART_RESET_BLUE)
    {
        stateEventTableMachine2[BLUE_STATE4_M2][EVENT2_M2]();   //event2 state4 machine2
    }
}

//STATE MACHINE 3

void SM_State1Machine3(void) 
{
}

void SM_State2Machine3(void)
{
}

void SM_State3Machine3(void)
{
}

void SM_State4Machine3(void)
{
}

//STATE MACHINE 4

void SM_State1Machine4(void)
{
}

void SM_State2Machine4(void)
{
}

void SM_State3Machine4(void)
{
}

void SM_State4Machine4(void)
{
}

//***********************************************************************************
//***************************** STATIC FUNCTIONS ************************************
//***********************************************************************************

//****************STATE MACHINE 1************************************

static void m_machine1State1Event1(void)  //red state1 --> red state2
{
    uint16_t redTimerId = LedGetRedTimerId();
     
    TmrStop(redTimerId);        //stop regardless if on or off
    
    if (TmrChangeTimerValue(redTimerId, QUARTER_SECOND))
    {
        const char* str = "Red State2\r\n\n";
        size_t strSize = strlen(str);
        HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);     
        
        LedRedTurnOff();
        TmrStart(redTimerId);
        SM_StateMachineRed.States = RED_STATE2_M1;
        SM_StateMachineRed.FuncPtr = SM_State2Machine1;
    }
}

static void m_machine1State1Event2(void)  //red state1 --> no transitions for now
{

}

static void m_machine1State2Event1(void)  //red state2 --> red state3
{
    uint16_t redTimerId = LedGetRedTimerId();
    
    TmrStop(redTimerId);
    
    if (TmrChangeTimerValue(redTimerId, HALF_SECOND))
    {  
        const char* str = "Red State3\r\n\n";
        size_t strSize = strlen(str);
        HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
        
        LedRedTurnOff();
        TmrStart(redTimerId);
        SM_StateMachineRed.States = RED_STATE3_M1;
        SM_StateMachineRed.FuncPtr = SM_State3Machine1;
    }
}

static void m_machine1State2Event2(void)  //red state2 -->  red state1
{
    m_gotoRedState1();  
}

static void m_machine1State3Event1(void)  //red state3 --> red state4
{
    uint16_t redTimerId = LedGetRedTimerId();
    
    TmrStop(redTimerId);
    
    if (TmrChangeTimerValue(redTimerId, ONE_SECOND))
    {  
        const char* str = "Red State4\r\n\n";
        size_t strSize = strlen(str);
        HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
        
        LedRedTurnOff();
        TmrStart(redTimerId);
        SM_StateMachineRed.States = RED_STATE4_M1;
        SM_StateMachineRed.FuncPtr = SM_State4Machine1;    
    }
}

static void m_machine1State3Event2(void)  //red state3 --> red state1
{
    m_gotoRedState1();  
}

static void m_machine1State4Event1(void) //red state4 --> red state1
{
    m_gotoRedState1();  
}

static void m_machine1State4Event2(void)  //red state4 --> red state1
{  
    m_gotoRedState1();
}

//****************STATE MACHINE 2************************************

static void m_machine2State1Event1(void) //blue state1 --> blue state2
{ 
    uint16_t blueTimerId = LedGetBlueTimerId();
     
    TmrStop(blueTimerId);        //stop regardless if on or off
    
    if (TmrChangeTimerValue(blueTimerId, QUARTER_SECOND))
    {
        const char* str = "Blue State2\r\n\n";
        size_t strSize = strlen(str);
        HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);     
        
        LedBlueTurnOff();
        TmrStart(blueTimerId);
        SM_StateMachineBlue.States = BLUE_STATE2_M2;
        SM_StateMachineBlue.FuncPtr = SM_State2Machine2;
    }
}

static void m_machine2State1Event2(void) //blue state1 --> no transitions for now
{
  
}

static void m_machine2State2Event1(void)  //blue state2 --> blue state3
{
    uint16_t blueTimerId = LedGetBlueTimerId();
    
    TmrStop(blueTimerId);
    
    if (TmrChangeTimerValue(blueTimerId, HALF_SECOND))
    {  
        const char* str = "Blue State3\r\n\n";
        size_t strSize = strlen(str);
        HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
        
        LedBlueTurnOff();
        TmrStart(blueTimerId);
        SM_StateMachineBlue.States = BLUE_STATE3_M2;
        SM_StateMachineBlue.FuncPtr = SM_State3Machine2;
    }
}

static void m_machine2State2Event2(void)  //blue state2 --> no transitions for now
{
    m_gotoBlueState1();
}

static void m_machine2State3Event1(void)  //blue state3 --> blue state4
{
    uint16_t blueTimerId = LedGetBlueTimerId();
    
    TmrStop(blueTimerId);
    
    if (TmrChangeTimerValue(blueTimerId, ONE_SECOND))
    {  
        const char* str = "Blue State4\r\n\n";
        size_t strSize = strlen(str);
        HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
        
        LedBlueTurnOff();
        TmrStart(blueTimerId);
        SM_StateMachineBlue.States = BLUE_STATE4_M2;
        SM_StateMachineBlue.FuncPtr = SM_State4Machine2;    
    }
}

static void m_machine2State3Event2(void)  //blue state3 --> blue state1
{
    m_gotoBlueState1();     
}

static void m_machine2State4Event1(void) //orange state event1 --> red state
{
    m_gotoBlueState1(); 
}

static void m_machine2State4Event2(void) //orange state event2 --> Transition to orange state Mode 1
{
    m_gotoBlueState1(); 
}

//****************STATE MACHINE 3************************************

static void m_machine3State1Event1(void)
{
}

static void m_machine3State1Event2(void)
{
}

static void m_machine3State2Event1(void)
{
}

static void m_machine3State2Event2(void)
{
}

static void m_machine3State3Event1(void)
{
}

static void m_machine3State3Event2(void)
{
}

static void m_machine3State4Event1(void)
{
}

static void m_machine3State4Event2(void)
{
}

//****************STATE MACHINE 4************************************

static void m_machine4State1Event1(void)
{
}

static void m_machine4State1Event2(void)
{
}

static void m_machine4State2Event1(void)
{
}

static void m_machine4State2Event2(void)
{
}

static void m_machine4State3Event1(void)
{
}

static void m_machine4State3Event2(void)
{
}

static void m_machine4State4Event1(void)
{
}

static void m_machine4State4Event2(void)
{
}

//****************Helper Functions************************************

static void m_gotoRedState1()
{
    uint16_t redTimerId = LedGetRedTimerId();
    
    TmrStop(redTimerId);
    
    const char* str = "Red State1\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    LedRedTurnOn();
    
    SM_StateMachineRed.States = RED_STATE1_M1;
    SM_StateMachineRed.FuncPtr = SM_State1Machine1;
}

static void m_gotoBlueState1()
{
    uint16_t blueTimerId = LedGetBlueTimerId();
    
    TmrStop(blueTimerId);
    
    const char* str = "Blue State1\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    LedBlueTurnOn();
    
    SM_StateMachineBlue.States = BLUE_STATE1_M2;
    SM_StateMachineBlue.FuncPtr = SM_State1Machine2;
}