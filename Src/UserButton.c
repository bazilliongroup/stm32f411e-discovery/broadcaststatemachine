/*
Library:			UserButton.c
Written by:			Ronald Bazillion
Date written:			07/03/2019
Description:			User Button functions
Resources:                      
            
Modifications:
-----------------

*/

//***** Header files *****//
#include "UserButton.h"
#include "TimerModule.h"
#include "BroadcastStateMachine.h"
#include "main.h"

extern FUNCT_PTR SM_StateMachine;

//***** Defines ********//

//***** typedefs enums *****//

//***** typedefs structs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
static bool m_usrButtonPressed;
static volatile bool m_usrButtonDebounce;
static volatile uint8_t m_tmrBtnDebounceId;
static bool m_usrDelayEnable;
static bool m_usrButtonDelay;
static volatile uint16_t m_usrButtonDelayValue;
static volatile uint8_t m_usrButtonDelayId;

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void UserButtonInit(void)
{    
    m_usrButtonPressed = false;
    m_usrButtonDebounce = false;
    m_usrDelayEnable = false;
    m_usrButtonDelay = false;
    m_usrButtonDelayValue = 0;
    m_usrButtonDelayId = UNSUCCESSFUL;
    
    m_tmrBtnDebounceId  = TmrCreate();
    if (m_tmrBtnDebounceId != UNSUCCESSFUL)
    {
      if (TmrPopulate(m_tmrBtnDebounceId, 250, UserButtonDbnCheck, 0, true))
      {
          TmrReset(m_tmrBtnDebounceId);
      }
      else
      {
        TmrDelete(m_tmrBtnDebounceId);
      }
    }
}

void UserButtonCheck(void)
{
    if ((m_usrButtonPressed == false) &&  //previous button state is not pressed
        (m_usrButtonDebounce == false) && //debounce not set
        (HAL_GPIO_ReadPin(UserButton_GPIO_Port, UserButton_Pin) == GPIO_PIN_SET)) //current button is being pressed
      {
            m_usrButtonPressed = true;    //set button as being presssed
            TmrReset(m_tmrBtnDebounceId);   //reset debounce
            TmrStart(m_tmrBtnDebounceId);   //start debounce
      }
}

void UserButtonDbnCheck(uint8_t var)
{
  if (HAL_GPIO_ReadPin(UserButton_GPIO_Port, UserButton_Pin) == GPIO_PIN_SET)
  {
    m_usrButtonDebounce = true;           //debounce is true
  }
  else
  {
    m_usrButtonDebounce = false;
    m_usrButtonPressed = false;  
  }
}

void UserButtonSetPressed(bool val)
{
    m_usrButtonPressed = val;
}
          
bool UserButtonGetPressed(void)
{
  bool ret = false;
  if (m_usrButtonPressed == true)
  {
    ret = true;
  }
  return ret;
}

void UserButtonSetDbn(bool val)
{
    m_usrButtonDebounce = val;
}

bool UserButtonGetDbn(void)
{
  bool ret = false;
  if (m_usrButtonDebounce == true)
  {
    ret = true;
  }
  return ret;
}     

bool UserButtonDelayEnable(uint16_t delayValue)
{  
  bool ret = false;
  if (m_usrButtonDelayId == UNSUCCESSFUL) //UNSUCCESSFUL is a default value.    
  {
     m_usrDelayEnable = true;
     m_usrButtonDelayValue = delayValue;
     m_usrButtonDelayId = TmrCreateTimer(m_usrButtonDelayValue, UserButtonDelayExp, true);
     if (m_usrButtonDelayId != UNSUCCESSFUL)
     {
        ret = true; //successful case.
     }
          
     if (ret == false)
     {
        m_usrDelayEnable = false;
        m_usrButtonDelayValue = 0;
     }
  }
  
  return ret;
}

void UserButtonDelayExp(uint8_t var)
{
    m_usrButtonDelay = false;
}

bool UserButtonIsDelayed(void)
{
    return m_usrButtonDelay;
}

bool UserButtonDelayDisable(void)
{
    if(m_usrButtonDelayId == UNSUCCESSFUL)
        return false;
    else
    {
        m_usrDelayEnable = false;
        m_usrButtonDelayValue = 0;
        TmrStop(m_usrButtonDelayId);
        TmrDelete(m_usrButtonDelayId);
        return true;
    }
}

bool UserButtonStartDelay(void)
{
    if (m_usrDelayEnable)
    {
        m_usrButtonDelay = true;
        TmrReset(m_usrButtonDelayId);     //start delay
        TmrStart(m_usrButtonDelayId);
        return true;
    }  
    return false;
}

//***********************************************************************************
//***************************** STATIC FUNCTIONS ************************************
//***********************************************************************************