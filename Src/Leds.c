/*
Library:			Leds.c
Written by:			Ronald Bazillion
Date written:			07/13/2019
Description:			Led API functions
Resources:                      
            
Modifications:
-----------------

*/

//***** Header files *****//
#include "Leds.h"
#include "TimerModule.h"
#include "main.h"

//***** Defines ********//

//***** typedefs enums *****//

//***** typedefs structs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
static uint8_t m_tmrRedBlinkingId = UNSUCCESSFUL;
static uint8_t m_tmrBlueBlinkingId = UNSUCCESSFUL;
static uint8_t m_tmrGreenBlinkingId = UNSUCCESSFUL;
static uint8_t m_tmrOrangeBlinkingId = UNSUCCESSFUL;

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void LedInit(void)
{
  m_tmrRedBlinkingId = TmrCreateTimer(1000, LedRedFunction, false);
  m_tmrBlueBlinkingId = TmrCreateTimer(1000, LedBlueFunction, false);
  m_tmrGreenBlinkingId = TmrCreateTimer(1000, LedGreenFunction, false);
  m_tmrOrangeBlinkingId = TmrCreateTimer(1000, LedOrangeFunction, false);
}

void LedRedFunction(uint8_t var)
{
    HAL_GPIO_TogglePin(RedLED_GPIO_Port, RedLED_Pin);
}

void LedBlueFunction(uint8_t var)
{
    HAL_GPIO_TogglePin(BlueLED_GPIO_Port, BlueLED_Pin);
}

void LedGreenFunction(uint8_t var)
{
    HAL_GPIO_TogglePin(GreenLED_GPIO_Port, GreenLED_Pin);
}

void LedOrangeFunction(uint8_t var)
{
    HAL_GPIO_TogglePin(OrangeLED_GPIO_Port, OrangeLED_Pin);
}

void LedRedTurnOn()
{
    HAL_GPIO_WritePin(RedLED_GPIO_Port, RedLED_Pin, GPIO_PIN_SET);
}

void LedRedTurnOff()
{
    HAL_GPIO_WritePin(RedLED_GPIO_Port, RedLED_Pin, GPIO_PIN_RESET);
}

void LedBlueTurnOn()
{
    HAL_GPIO_WritePin(BlueLED_GPIO_Port, BlueLED_Pin, GPIO_PIN_SET);
}

void LedBlueTurnOff()
{
    HAL_GPIO_WritePin(BlueLED_GPIO_Port, BlueLED_Pin, GPIO_PIN_RESET);
}

void LedGreenTurnOn()
{
    HAL_GPIO_WritePin(GreenLED_GPIO_Port, GreenLED_Pin, GPIO_PIN_SET);
}

void LedGreenTurnOff()
{
    HAL_GPIO_WritePin(GreenLED_GPIO_Port, GreenLED_Pin, GPIO_PIN_RESET);
}

void LedOrangeTurnOn()
{
    HAL_GPIO_WritePin(OrangeLED_GPIO_Port, OrangeLED_Pin, GPIO_PIN_SET);
}

void LedOrangeTurnOff()
{
    HAL_GPIO_WritePin(OrangeLED_GPIO_Port, OrangeLED_Pin, GPIO_PIN_RESET);
}

uint8_t LedGetRedTimerId()
{
  return m_tmrRedBlinkingId;
}

uint8_t LedGetBlueTimerId()
{
  return m_tmrBlueBlinkingId;
}

uint8_t LedGetGreenTimerId()
{
  return m_tmrGreenBlinkingId;
}

uint8_t LedGetOrangeTimerId()
{
  return m_tmrOrangeBlinkingId;
}

//***********************************************************************************
//***************************** STATIC FUNCTIONS ************************************
//***********************************************************************************