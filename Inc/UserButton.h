/*
Library:			UserButton.h
Written by:			Ronald Bazillion
Date written:			07/03/2019
Description:			User Button functions
Resources:                      
            
Modifications:
-----------------

*/

#ifndef __USER_BUTTON_H_
#define __USER_BUTTON_H_

//***** Header files *****//
#include "stm32f4xx_hal.h"
#include <stdbool.h>

//***** Defines ********//

//*******Enums*****************//

//*******Flags****************//

//*****Structures *************//

//***** Constant Variables *****//

//***** API Functions prototype *****//

/* Name: UserButtonInit(void) 
   Description: Initialize the user Button
   Inputs args: None
   Output args: None 
*/
void UserButtonInit(void);

/* Name: UserButtonCheck(void) 
   Description: Function used to check the user button. If button pressed it will initilaize the debounce timer.
   Inputs args: None
   Output args: None 
*/
void UserButtonCheck(void);

/* Name: UserButtonDbnCheck(uint8_t var) 
   Description: debounce timer interrupt function.
   Inputs args: var - necessary for the interrupt function format allows you to add in an 8 bit value.
   Output args: None 
*/
void UserButtonDbnCheck(uint8_t var);

/* Name: UserButtonSetPressed(bool val) 
   Description: sets the User Button Pressed value
   Inputs args: var - boolean value used to set variable.
   Output args: None 
*/
void UserButtonSetPressed(bool val);

/* Name: UserButtonGetPressed(void) 
   Description: gets the User Button Pressed value
   Inputs args: None
   Output args: true - user button pressed, false - user button not pressed
*/
bool UserButtonGetPressed(void);

/* Name: UserButtonSetPressed(bool val) 
   Description: sets the User Button Debounced value
   Inputs args: var - boolean value used to set variable.
   Output args: None 
*/
void UserButtonSetDbn(bool val);

/* Name: UserButtonGetDbn(void) 
   Description: gets the User Button Debounced value
   Inputs args: None
   Output args: true - user button pressed, false - user button not pressed
*/
bool UserButtonGetDbn(void);

/* Name: UserButtonDelayEnable(uint16_t delayValue) 
   Description: Used to enable the delay feature
   Inputs args: delayValue - delay value 16 bit number.
   Output args: true if successful false if not.

   NOTE: this function will return false if done more than once, to reenable
         you must disable the delay feature first than reenable.

         The delay functionality is used in cas you want to have a debounce time and 
         a delay time so you can do either long button presses or ensuring multiple
         buttons aren't pressed after the debounce. You can also increase the debounce
         but this gives you the option of a debounce and also a delay.
*/
bool UserButtonDelayEnable(uint16_t delayValue);

/* Name: UserButtonDelayExp(uint8_t var) 
   Description: Timer call back for User button delay
   Inputs args: var - boolean value used to set variable.
   Output args: None
*/
void UserButtonDelayExp(uint8_t var);

/* Name: UserButtonIsDelayed(void) 
   Description: Used to see if the delay is still on.
   Inputs args: None
   Output args: None
*/
bool UserButtonIsDelayed(void);

/* Name: UserButtonDelayDisable(void) 
   Description: Used to disable the delay feature
   Inputs args: None
   Output args: true if successful false if not.

   NOTE: This function will disable the delay feature allowing you to 
         reenable it. This will also destroy the timerId used for the delay
*/
bool UserButtonDelayDisable(void);

bool UserButtonStartDelay(void);

#endif