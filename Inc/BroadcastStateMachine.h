/*
Library:			BroadcastStateMachine.h
Written by:			Ronald Bazillion
Date written:			07/02/2019
Description:			Header File for Broadcast State Machine
Resources:                      https://www.youtube.com/watch?v=pxaIyNbcPrA&index=9&list=PL5701E4325336503E&t=53s
                                https://www.youtube.com/playlist?list=PLn8PRpmsu08pHsOznk_WT6QT9cySw1A59
            
Modifications:
-----------------

*/

#ifndef __BASIC_STATE_MACHINE_H_
#define __BASIC_STATE_MACHINE_H_

//*******Include*****************//
#include "stm32f4xx_hal.h"

//*******Defines*****************//
#define MAX_STATES_MACH1 4
#define MAX_EVENTS_MACH1 2
#define MAX_STATES_MACH2 4
#define MAX_EVENTS_MACH2 2
#define MAX_STATES_MACH3 4
#define MAX_EVENTS_MACH3 2
#define MAX_STATES_MACH4 4
#define MAX_EVENTS_MACH4 2
//*******Enums*****************//

typedef enum
{
//********MACHINE 1***********  
  RED_STATE1_M1 = 0,
  RED_STATE2_M1 = 1, 
  RED_STATE3_M1 = 2,
  RED_STATE4_M1 = 3,
//********MACHINE 2*********** 
  BLUE_STATE1_M2 = 0,
  BLUE_STATE2_M2 = 1, 
  BLUE_STATE3_M2 = 2,
  BLUE_STATE4_M2 = 3,  
//********MACHINE 3***********  
  GREEN_STATE1_M3 = 0,
  GREEN_STATE2_M3 = 1, 
  GREEN_STATE3_M3 = 2,
  GREEN_STATE4_M3 = 3,
//********MACHINE 4*********** 
  ORANGE_STATE1_M4 = 0,
  ORANGE_STATE2_M4 = 1, 
  ORANGE_STATE3_M4 = 2,
  ORANGE_STATE4_M4 = 3,    
}BASIC_STATES;

typedef enum
{
//********EVENTS FOR MACH 1***********  
  EVENT1_M1 = 0,
  EVENT2_M1 = 1, 
//********EVENTS FOR MACH 2***********  
  EVENT1_M2 = 0,
  EVENT2_M2 = 1,
//********EVENTS FOR MACH 3***********  
  EVENT1_M3 = 0,
  EVENT2_M3 = 1, 
//********EVENTS FOR MACH 4***********  
  EVENT1_M4 = 0,
  EVENT2_M4 = 1,  
}BASIC_EVENTS;

//*******Flags****************//

//*****Structures *************//

typedef void (*FUNCT_PTR)(void);

typedef struct
{
  BASIC_STATES States;
  BASIC_EVENTS Events;
  FUNCT_PTR FuncPtr;            // My function Pointer to the state functions.        
}STATE_MACHINE_STRUCT;

//***** Constant Variables *****//

//***** API Functions prototype *****//

/* Name: SM_Init(void) 
   Description: Used to initialize the State Machine
   Inputs args: None
   Output args: None 
*/
void SM_Init(UART_HandleTypeDef* huart);

/* Name: SM_CommonTasks(void) 
   Description: entry POint for common functions that will be called regardless of any state.
   Inputs args: None
   Output args: None 
*/
void SM_CommonTasks(void);

//state machine functions
void SM_State1Machine1(void);
void SM_State2Machine1(void);
void SM_State3Machine1(void);
void SM_State4Machine1(void);

void SM_State1Machine2(void);
void SM_State2Machine2(void);
void SM_State3Machine2(void);
void SM_State4Machine2(void);

void SM_State1Machine3(void);
void SM_State2Machine3(void);
void SM_State3Machine3(void);
void SM_State4Machine3(void);

void SM_State1Machine4(void);
void SM_State2Machine4(void);
void SM_State3Machine4(void);
void SM_State4Machine4(void);

#endif